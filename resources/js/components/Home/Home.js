import React, {Component} from 'react'
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import {Link} from "react-router-dom";
class Home extends Component {
  constructor() {
    super();
    this.state = {
      isLoggedIn: false,
      user: {}
    }
  }
  // check if user is authenticated and storing authentication data as states if true
  componentWillMount() {
    let state = localStorage["appState"];
    if (state) {
      let AppState = JSON.parse(state);
      this.setState({ isLoggedIn: AppState.isLoggedIn, user: AppState.user });
    }
  }
render() {
    return (
      <div className="container">
          <div className="row">
              <div className="col-md-12">
                  <h1 className="mt-5 text-center">Welcome to MiniBlog</h1>
                  <h3 className="mt-5 text-center"><Link className="nav-link" to="/post">Posts</Link></h3>
              </div>
          </div>
      </div>
      )
    }
  }
export default Home
