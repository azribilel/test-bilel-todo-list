import React, {Component} from 'react'
import {Link, withRouter} from 'react-router-dom';
class Header extends Component {
  // 1.1
  constructor(props) {
    super(props);
      this.state = {
        user: props.userData,
        isLoggedIn: props.userIsLoggedIn
      };
      this.logOut = this.logOut.bind(this);
  }
  // 1.2
  logOut() {
    let appState = {
      isLoggedIn: false,
      user: {}
    };
    localStorage["appState"] = JSON.stringify(appState);
    this.setState(appState);
    location.reload();
  }
  // 1.3
  render() {
    const aStyle = {
      cursor: 'pointer'
    };

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
            <Link to="/" className="navbar-brand" href="#">MiniBlog</Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav ml-auto">
                    {this.state.isLoggedIn ?
                        <li className="nav-item"><Link className="nav-link" to="/post">Posts</Link></li> : ""}
                    {this.state.isLoggedIn ?
                        <li className="nav-item"><a className="nav-link" href="#" onClick={this.logOut}>Logout</a></li> : ""}
                    {!this.state.isLoggedIn ?
                        <li className="nav-item"><Link className="nav-link" to="/login">Login</Link></li> : ""}
                    {!this.state.isLoggedIn ?
                        <li className="nav-item"><Link className="nav-link" to="/register">Register</Link></li> : ""}
                </ul>
            </div>
        </nav>
    )
  }
}
export default withRouter(Header)
