import React, {Component} from 'react';
import PostCommentItem from './PostCommentItem';
import {Link} from "react-router-dom";

let count = 0;

class PostItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: false,
            user: {},
            commentData: {
                content: ''
            },
            comments: props.post.comments
        };
        this.guid = 'post-ui-' + count++;
        this.handleComment = this.handleComment.bind(this);
        this.handleContent = this.handleContent.bind(this);
    }

    componentWillMount() {
        let state = localStorage["appState"];
        if (state) {
            let AppState = JSON.parse(state);
            this.setState({ isLoggedIn: AppState.isLoggedIn, user: AppState.user });
        }
    }

    deletePost(id) {
        axios.delete(`/api/post/destroy/${id}`, {
            headers: {
                'Authorization': `Bearer ${this.state.user.token}`
            }
        })
            .then(response => {
                document.getElementById(this.guid).remove()
            })
    }

    handleComment(e) {
        e.preventDefault();
        this.setState({formSubmitting: true});
        let commentData = this.state.commentData;
        axios.post("/api/post/comment/store", commentData, {
            headers: {
                'Authorization': `Bearer ${this.state.user.token}`
            }
        }).then(response => {
            return response;
        }).then(json => {
            if (json.data.status === 'success') {
                let commentData = {
                    id: json.data.comment.id,
                    content: json.data.comment.content,
                    user_id: json.data.comment.user_id,
                    post_id: json.data.comment.post_id,
                    updated_at: json.data.comment.updated_at,
                    created_at: json.data.comment.created_at,
                    user: this.state.user
                };

                let comments = this.state.comments;
                comments.unshift(commentData);
                let appState = {
                    isLoggedIn: this.state.isLoggedIn,
                    user: this.state.user,
                    comment: {},
                    formSubmitting: false,
                    commentData: {
                        content: ''
                    },
                    comments: comments
                };

                this.setState(appState);

            } else {
                alert(`Our System Failed To Publish Your Post!`);
            }
        }).catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code that falls out of the range of 2xx
                let err = error.response.data;
                this.setState({
                    error: err.message,
                    errorMessage: err.errors,
                    formSubmitting: false
                })
            } else if (error.request) {
                // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                let err = error.request;
                this.setState({
                    error: err,
                    formSubmitting: false
                })
            } else {
                // Something happened in setting up the request that triggered an Error
                let err = error.message;
                this.setState({
                    error: err,
                    formSubmitting: false
                })
            }
        }).finally(this.setState({error: ''}));
    }

    handleContent(e) {
        let value = e.target.value;
        this.setState({
            commentData: {
                user_id: this.state.user.id, post_id: this.props.post.id, content: value
            }
        });
    }

    render() {
        return (
            <div className="col-md-12 mt-5" id={this.guid}>
                <div className="card">
                    <div className="card-header">
                        [{this.props.post.created_at}] <strong>{this.props.post.user.name}</strong>
                        {this.state.user.id === this.props.post.user.id ? <Link to="#" className="text-danger float-right" onClick={this.deletePost.bind(this, this.props.post.id)}>Delete</Link>: ""}
                    </div>
                    <div className="card-body">
                        <p className="card-text">{this.props.post.content}</p>
                        <hr/>
                        {this.state.comments.length > 0 ? <h6><strong>Comments</strong></h6> : ""}
                        {this.state.comments.map((comment, i) => {
                            return (<PostCommentItem key={i} comment={comment}/>)
                        })}
                        <div className="col-md-12 mt-3">
                            <form onSubmit={this.handleComment}>
                                    <textarea id="post_content" name="content" className="form-control" required
                                              placeholder="Enter your comment" value={this.state.commentData.content} onChange={this.handleContent}/>
                                <button disabled={this.state.formSubmitting} type="submit" name="comment_submit"
                                        className="btn btn-primary mt-5 float-right">{this.state.formSubmitting ? "Publishing..." : "Publish"}
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default PostItem
