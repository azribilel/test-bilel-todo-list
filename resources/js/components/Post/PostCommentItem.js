import React, {Component} from 'react';
import {Link} from "react-router-dom";

let count = 0;

class PostCommentItem extends Component {
    constructor(props) {
        super(props);
        this.guid = 'comment-ui-' + count++;
    }
    componentWillMount() {
        let state = localStorage["appState"];
        if (state) {
            let AppState = JSON.parse(state);
            this.setState({ isLoggedIn: AppState.isLoggedIn, user: AppState.user });
        }
    }
    deleteComment(id) {
        axios.delete(`/api/post/comment/destroy/${id}`, {
            headers: {
                'Authorization': `Bearer ${this.state.user.token}`
            }
        })
            .then(response => {
                document.getElementById(this.guid).remove()
            })
    }
    render() {
        return (
            <div className="col-md-12 mt-3" id={this.guid}>
                <div className="card  p-3">
                    <p className="card-text"><strong>{this.props.comment.user.name}</strong>&nbsp;{this.props.comment.content}</p>
                    <p className="card-text">{this.props.comment.created_at}
                        {this.state.user.id === this.props.comment.user.id ? <span>&nbsp;-&nbsp; <Link to="#" className="text-info" onClick={this.deleteComment.bind(this, this.props.comment.id)}>Delete</Link> </span>: ""}
                    </p>
                </div>
            </div>
        )
    }
}

export default PostCommentItem
