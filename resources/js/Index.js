import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import Main from './Router';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
class Index extends Component {
    constructor() {
        super();
        this.state = {
            isLoggedIn: false,
            user: {}
        }
    }
    // check if user is authenticated and storing authentication data as states if true
    componentWillMount() {
        let state = localStorage["appState"];
        if (state) {
            let AppState = JSON.parse(state);
            this.setState({ isLoggedIn: AppState.isLoggedIn, user: AppState.user });
        }
    }
  render() {
    return (
      <BrowserRouter>
          <Header userData={this.state.user} userIsLoggedIn={this.state.isLoggedIn}/>
          <div className="container">
              <Route component={Main} />
          </div>
          <Footer />
      </BrowserRouter>
    );
  }
}
ReactDOM.render(<Index/>, document.getElementById('index'));
