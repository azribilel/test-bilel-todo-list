import React, {Component} from 'react';
import PostContainer from './PostContainer';
import {withRouter} from "react-router-dom";
class Post extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: props.location,
        };
    }
    render() {
        return (
            <div className="content">
                <PostContainer redirect={this.state.redirect} />
            </div>
        )
    }
}
export default withRouter(Post)
