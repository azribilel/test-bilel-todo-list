import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import PostItem from '../../../components/Post/PostItem';
import ReactDOM from "react-dom";
class PostContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: false,
            user: {},
            postData: {
                content: ''
            },
            posts: [],
            comment: {
                content: ''
            }
        };
        this.handlePost = this.handlePost.bind(this);
        this.handleContent = this.handleContent.bind(this);

    }

    componentWillMount() {
        let state = localStorage["appState"];
        if (state) {
            let AppState = JSON.parse(state);
            this.setState({isLoggedIn: AppState.isLoggedIn, user: AppState.user});
        }
    }

    componentDidMount() {
        axios.get('/api/post/index', {
            headers: {
                'Authorization': `Bearer ${this.state.user.token}`
            }
        })
            .then(response => {
                this.setState({
                    posts: response.data.posts
                })
            })
    }

    handlePost(e) {
        e.preventDefault();
        this.setState({formSubmitting: true});
        let postData = this.state.postData;
        axios.post("/api/post/store", postData, {
            headers: {
                'Authorization': `Bearer ${this.state.user.token}`
            }
        }).then(response => {
            return response;
        }).then(json => {
            if (json.data.status === 'success') {
                let postData = {
                    id: json.data.post.id,
                    content: json.data.post.content,
                    user_id: json.data.post.user,
                    updated_at: json.data.post.updated_at,
                    created_at: json.data.post.created_at,
                    comments: [],
                    user: this.state.user
                };

                let posts = this.state.posts;
                posts.unshift(postData);
                let appState = {
                    isLoggedIn: this.state.isLoggedIn,
                    user: this.state.user,
                    postData: {
                        content: ''
                    },
                    posts: posts,
                    comment: {},
                    formSubmitting: false,
                };

                this.setState(appState);


            } else {
                alert(`Our System Failed To Publish Your Post!`);
            }
        }).catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code that falls out of the range of 2xx
                let err = error.response.data;
                this.setState({
                    error: err.message,
                    errorMessage: err.errors,
                    formSubmitting: false
                })
            } else if (error.request) {
                // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                let err = error.request;
                this.setState({
                    error: err,
                    formSubmitting: false
                })
            } else {
                // Something happened in setting up the request that triggered an Error
                let err = error.message;
                this.setState({
                    error: err,
                    formSubmitting: false
                })
            }
        }).finally(this.setState({error: ''}));
    }

    handleContent(e) {
        let value = e.target.value;
        this.setState({
            postData: {
                user_id: this.state.user.id, content: value
            }
        });
    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-header">
                                Hi, {this.state.user.name}. what's new ?
                            </div>
                            <div className="card-body">
                                <form onSubmit={this.handlePost}>
                                    <textarea id="post_content" name="content" className="form-control" required
                                              placeholder="Enter your post" value={this.state.postData.content} onChange={this.handleContent}/>
                                    <button disabled={this.state.formSubmitting} type="submit" name="post_submit"
                                            className="btn btn-primary mt-5 float-right">{this.state.formSubmitting ? "Publishing..." : "Publish"}
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    {this.state.posts.map((post, i) => {
                        return (<PostItem key={i} post={post}/>)
                    })}
                </div>
            </div>
        )
    }
}

export default withRouter(PostContainer);
