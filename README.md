# Test Mini Blog 
Installation steps: 

1. Clone the repo
```
git clone git@gitlab.com:azribilel/test-bilel-todo-list.git
```

2. Enter the project folder
```
cd test-bilel-todo-list
```

3. Build docker containers
```
docker-compose build
```

4. Run docker containers
```
docker-compose up -d
```

5. Run migrations
```
docker-compose exec php-fpm php artisan migrate
```

6. Open project in navigator

    [Mini Blog](http://127.0.0.1).

