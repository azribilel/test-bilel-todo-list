<?php

/*
 *
 * @author Bilel Azri    <azri.bilel@gmail.com>
 *
 * Infinity Management (c) 2021-present.
 */

namespace App\Http\Controllers\Api\Post;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): Response
    {
        $posts = Post::with(['comments' => function ($query) {
            $query->with('user')->orderBy('created_at', 'DESC');
        }, 'user'])->orderBy('created_at', 'DESC')->get();

        return response()->json(['status' => 'success', 'posts' => $posts], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'user_id' => 'required',
        ]);

        $post = Post::create($request->all());

        return response()->json(['status' => 'success',  'message' => 'Post created successfully', 'post' => $post], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @return Response
     */
    public function destroy(Post $post): Response
    {
        try {
            if (Auth::user()->id === $post->user->id) {
                $post->delete();

                return response()->json(['status' => 'success', 'message' => 'Post deleted successfully'], Response::HTTP_OK);
            }
        } catch (\Throwable $exception) {
        }

        return response()->json(['status' => 'failed', 'message' => 'Unauthorized!'], Response::HTTP_UNAUTHORIZED);
    }
}
