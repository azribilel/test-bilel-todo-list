<?php

/*
 *
 * @author Bilel Azri    <azri.bilel@gmail.com>
 *
 * Infinity Management (c) 2021-present.
 */

namespace App\Http\Controllers\Api\Post;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class PostCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post): Response
    {
        $PostComments = PostComment::all();

        return response()->json(['status' => 'success', 'comments' => $PostComments], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'user_id' => 'required',
            'post_id' => 'required',
        ]);

        $PostComment = PostComment::create($request->all());

        return response()->json(['status' => 'success', 'message' => 'PostComment created successfully', 'comment' => $PostComment], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  PostComment  $PostComment
     * @return Response
     */
    public function destroy(PostComment $postComment): Response
    {
        try {
            if (Auth::user()->id === $postComment->user->id) {
                $postComment->delete();

                return response()->json(['status' => 'success', 'message' => 'PostComment deleted successfully'], Response::HTTP_OK);
            }
        } catch (\Throwable $exception) {
        }

        return response()->json(['status' => 'failed', 'message' => 'Unauthorized!'], Response::HTTP_UNAUTHORIZED);
    }
}
