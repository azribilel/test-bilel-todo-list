<?php

/*
 *
 * @author Bilel Azri    <azri.bilel@gmail.com>
 *
 * Infinity Management (c) 2021-present.
 */

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class RegisterController extends Controller
{
    use RegistersUsers;

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => 'Registration error', 'errors' => $validator->messages()], Response::HTTP_BAD_REQUEST);
        }

        try {
            $user = User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
            ]);
        } catch (QueryException $exception) {
            return response()->json(['status' => 'failed', 'message' => 'Registration error', 'errors' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $this->guard()->login($user);
        $success['token'] = $user->createToken('MiniBlog')->accessToken;
        $success['user'] = $user;
        $success['status'] = 'success';

        return response()->json($success, Response::HTTP_OK);
    }
}
