<?php

/*
 *
 * @author Bilel Azri    <azri.bilel@gmail.com>
 *
 * Infinity Management (c) 2021-present.
 */

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
}
