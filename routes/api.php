<?php

/*
 *
 * @author Bilel Azri    <azri.bilel@gmail.com>
 *
 * Infinity Management (c) 2021-present.
 */

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', 'Api\\Auth\\LoginController@login');
    Route::post('register', 'Api\\Auth\\RegisterController@register');
});

//protected routes
Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('logout', 'Api\\Auth\\LoginController@logout');

    Route::group([
        'prefix' => 'post',
    ], function () {
        Route::get('index', 'Api\\Post\\PostController@index');
        Route::post('store', 'Api\\Post\\PostController@store');
        Route::delete('destroy/{post}', 'Api\\Post\\PostController@destroy');

        Route::group([
            'prefix' => 'comment',
        ], function () {
            Route::get('index/{post}', 'Api\\Post\\PostCommentController@index');
            Route::post('store', 'Api\\Post\\PostCommentController@store');
            Route::delete('destroy/{postComment}', 'Api\\Post\\PostCommentController@destroy');
        });
    });
});
